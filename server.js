placematUsers = []

const env = process.env.NODE_ENV || 'development';
const config = require('./config')[env];
const { PlacematUser } = require("./PlacematUser");
const express = require('express')
//const request = require('request')
//const cookieParser = require('cookie-parser')
const app = express()
app.set('view engine', 'ejs')
app.use(express.static('public'))
const bodyParser = require('body-parser')
app.use(bodyParser.urlencoded({extended: true}))
const server = app.listen(process.env.PORT || 3000, () => {
    console.log('Server lauscht auf Port %s', server.address().port)    
})

const mysql = require('mysql')
const dbVerbindung = mysql.createConnection({
    host: config.database.host,
    user: config.database.user,
    password: config.database.password,
    database: config.database.db
})

dbVerbindung.connect()

function anzeigen(placematUser, endeUhrzeitNachdenken, endeUhrzeitVergleichen, endeUhrzeitKonsens){
    
    let treffpunkt = "bei " + placematUser.treffpunkt
    
    if(placematUser.name === placematUser.treffpunkt){
        treffpunkt = "bei Dir"
    }
    
    let anzeigen = [ "<tr><th></th><th>Wann?</th><th>Wo?</th><th>Was?</th></tr>"]                    
    anzeigen.push("<tr><td><h4>1.</h4></td><td>jetzt</td><td>bei Dir</td><td><p>Dein Thema lautet:</p><p><b><a target='_blank' href='" + placematUser.url + "'> " + placematUser.thema + "</a></b></p><p>Die genaue Anweisung findest Du <a target='_blank' href='" + placematUser.url + "'>hier</a>.</p></td></tr>")     
    anzeigen.push("<tr><td><h4>2.</h4></td><td>" + ("0" + endeUhrzeitNachdenken.getHours()).slice(-2) +":" + ("0" + endeUhrzeitNachdenken.getMinutes()).slice(-2) + " Uhr</td><td>" + treffpunkt + "</td><td>Du liest die Notizen derjenigen, die sich " + treffpunkt + " eingefunden haben.</td></tr>")         
    anzeigen.push("<tr><td><h4>3.</h4></td><td>" + ("0" + endeUhrzeitVergleichen.getHours()).slice(-2) +":" + ("0" + endeUhrzeitVergleichen.getMinutes()).slice(-2) + " Uhr</td><td>" + treffpunkt + "</td><td>Du trägst <a target='_blank' href='" + placematUser.url + "'>hier</a> gemeinsam mit all denen, die sich " + treffpunkt + " eingefunden haben relevante Erkenntnisse zusammen.</td></tr>")    
    anzeigen.push("<tr><td><h4>P:</h4></td><td>" + ("0" + endeUhrzeitKonsens.getHours()).slice(-2) +":" + ("0" + endeUhrzeitKonsens.getMinutes()).slice(-2) + " Uhr</td><td>Plenum" + "</td><td>PÄSENTATION. Viel Spaß :-)</td></tr>")   

    return anzeigen
}

app.get('/',(req, res, next) => {        
    dbVerbindung.query("SELECT titel, themen, zeitstempel, (TIMESTAMPDIFF(SECOND, endeUhrzeitKonsens, NOW())) AS placematVorbeiSeitSekunden from placemat;", (err, rows) => { 
        if (err) return next(err)       
        
		if(rows[0] === undefined || rows[0].placematVorbeiSeitSekunden > 120){
            res.render('index.ejs', {                    
                welcome: "Zur Zeit kein aktives Placemat",
                anzeigen: "",
                endeUhrzeitNachdenken: new Date(),
                endeUhrzeitVergleichen: new Date(),
                endeUhrzeitKonsens: new Date()       
            })
        }else{
            res.render('index.ejs', {                    
                welcome: rows[0].titel + "</br> läuft seit " + ("0" + (rows[0].zeitstempel).getHours()).slice(-2) + ":" + ("0" + (rows[0].zeitstempel).getMinutes()).slice(-2)  + ":" + ("0" + (rows[0].zeitstempel).getSeconds()).slice(-2) + " Uhr.</br> Jetzt mitmachen!",
                anzeigen: "",
                endeUhrzeitNachdenken: new Date(),
                endeUhrzeitVergleichen: new Date(),
                endeUhrzeitKonsens: new Date()
            })
        }        
    })    
})

app.post('/', (req, res, next) => {    
    if(req.body.tbxName === "") return next(new Error("Der Name darf nicht leer sein."))    
    dbVerbindung.query("SELECT * from placemat;", (err, rows) => {
        if (err) return next(err)                     
        let urls = rows[0].urls
        let themen = rows[0].themen
        
        let maxProGruppe = rows[0].maxProGruppe
        let endeUhrzeitNachdenken = rows[0].endeUhrzeitNachdenken
        let endeUhrzeitVergleichen = rows[0].endeUhrzeitVergleichen
        let endeUhrzeitKonsens = rows[0].endeUhrzeitKonsens 
        
        
        /*dbVerbindung.query("SELECT * from placematuser;", (err, rows) => {
            if (err) return next(err)
            for (let row of rows) {                
                let placematUser = new PlacematUser()
                placematUser.name = row.name
                placematUser.gruppe = row.gruppe                
                placematUser.treffpunkt = row.treffpunkt
                placematUser.url = urls.split(',')[(placematUser.gruppe - 1) % urls.split(',').length] 
                placematUser.thema = themen.split(',')[(placematUser.gruppe - 1) % themen.split(',').length]
                
                if(row.name === req.body.tbxName){                    
                    res.render('index.ejs', {    
                        welcome: "Hallo, " + placematUser.name + "!", 
                        anzeigen: anzeigen(placematUser, endeUhrzeitNachdenken, endeUhrzeitVergleichen, endeUhrzeitKonsens),
                        endeUhrzeitNachdenken: endeUhrzeitNachdenken,
                        endeUhrzeitVergleichen: endeUhrzeitVergleichen,
                        endeUhrzeitKonsens: endeUhrzeitKonsens       
                    }) 
                    return
                }
                placematUsers.push(placematUser)
            }            */
            
            if((placematUsers.filter((p) => p.name === req.body.tbxName)).length === 1) {

                let pu = (placematUsers.filter((p) => p.name === req.body.tbxName))[0]
                
                res.render('index.ejs', {  
                    welcome: "Nochmal Hallo, " + pu.name + "!",      
                    anzeigen: anzeigen(pu, endeUhrzeitNachdenken, endeUhrzeitVergleichen, endeUhrzeitKonsens),    
                    endeUhrzeitNachdenken: endeUhrzeitNachdenken,
                    endeUhrzeitVergleichen: endeUhrzeitVergleichen,
                    endeUhrzeitKonsens: endeUhrzeitKonsens       
                })
                return
            }
            
            placematUsers.push(new PlacematUser())
            
            let anzahl = placematUsers.length - 1
            placematUsers[anzahl].name = req.body.tbxName                        
            placematUsers[anzahl].gruppe = parseInt(placematUsers.length % themen.split(',').length)            
            
            if(!placematUsers[anzahl].gruppe){                
                placematUsers[anzahl].gruppe = themen.split(',').length                
            }
            
            console.log(placematUsers[anzahl].name)
            console.log("Gruppe: " + (parseInt(placematUsers[anzahl].gruppe) + themen.split(',').length * parseInt((anzahl) / (themen.split(',').length * maxProGruppe))))
            console.log("Block: " + themen.split(',').length * parseInt((anzahl) / (themen.split(',').length * maxProGruppe)))

            placematUsers[anzahl].gruppe = (parseInt(placematUsers[anzahl].gruppe) + themen.split(',').length * parseInt((anzahl) / (themen.split(',').length * maxProGruppe)))
            placematUsers[anzahl].url = urls.split(',')[(placematUsers[anzahl].gruppe - 1) % urls.split(',').length] 
            placematUsers[anzahl].thema = themen.split(',')[(placematUsers[anzahl].gruppe - 1) % themen.split(',').length]
            
            console.log("URL: " + placematUsers[anzahl].url)
            console.log("Thema: " + placematUsers[anzahl].thema)

            // Der erste jeder Gruppe wird zum Treffpunkt

            if((placematUsers.filter((p) => p.gruppe === placematUsers[anzahl].gruppe)).length === 1) {
                placematUsers[anzahl].treffpunkt = placematUsers[anzahl].name
            }else{
                placematUsers[anzahl].treffpunkt = (placematUsers.filter((p) => p.gruppe === placematUsers[anzahl].gruppe))[0].treffpunkt
            }
            
            console.log("Treffpunkt: " + placematUsers[anzahl].treffpunkt)
            
            dbVerbindung.query("INSERT INTO placematuser(gruppe, name, thema, url, treffpunkt, zeitstempel) VALUES ('" + placematUsers[anzahl].gruppe + "','" + placematUsers[anzahl].name + "','" + placematUsers[anzahl].thema + "','" + placematUsers[anzahl].url + "','" + placematUsers[anzahl].treffpunkt + "' , NOW());", (err, result) => {                
                if (err) return next(err)                
                res.render('index.ejs', {  
                    welcome: "Hallo, " + placematUsers[anzahl].name + "!",      
                    anzeigen: anzeigen(placematUsers[anzahl], endeUhrzeitNachdenken, endeUhrzeitVergleichen, endeUhrzeitKonsens),    
                    endeUhrzeitNachdenken: endeUhrzeitNachdenken,
                    endeUhrzeitVergleichen: endeUhrzeitVergleichen,
                    endeUhrzeitKonsens: endeUhrzeitKonsens       
                })        
            })    
        //})
    })
})
app.use((err, req, res, next) => {    
    console.log(err.stack)
    res.render('error.ejs', {        
        error:["F E H L E R", err.message, "Falls Du nicht automatisch weitergeleitet wirst, dann ...", "Seite neu laden, um fortzufahren."]
    }) 
})


app.get('/admin', (req, res,next) => {    
    dbVerbindung.query("CREATE TABLE IF NOT EXISTS placemat(nummer INT AUTO_INCREMENT, zeitstempel TIMESTAMP, titel VARCHAR(50), themen VARCHAR(150), urls VARCHAR(200), maxProGruppe INT, dauerNachdenken INT, endeUhrzeitNachdenken DATETIME, dauerVergleichen INT, endeUhrzeitVergleichen DATETIME, dauerKonsens INT, endeUhrzeitKonsens DATETIME, PRIMARY KEY(nummer));", (err) => {
        if (err) return next(err)         
                
        console.log("Tabelle 'placemat' erfolgreich angelegt, bzw. schon vorhanden.");
    })

    dbVerbindung.query("CREATE TABLE IF NOT EXISTS placematUser(gruppe INT, name VARCHAR(50), thema VARCHAR(50), url VARCHAR(50), treffpunkt VARCHAR(50), zeitstempel TIMESTAMP, PRIMARY KEY(name,thema));", (err) => {        
        if (err) return next(err)         
        
        // Falls kein Fehler auftritt, wird der Erfolg geloggt.
        
        console.log("Tabelle 'placematUser' erfolgreich angelegt, bzw. schon vorhanden.");
    })

    dbVerbindung.query("CREATE TABLE IF NOT EXISTS users(username VARCHAR(50), password VARCHAR(50), PRIMARY KEY(username));", (err) => {        
        if (err) return next(err)         
        
        // Falls kein Fehler auftritt, wird der Erfolg geloggt.
        
        console.log("Tabelle 'users' erfolgreich angelegt, bzw. schon vorhanden.");
    })

    // Das zuletzt angelegte Placemat wird selektiert
    
    dbVerbindung.query("SELECT *, (TIMESTAMPDIFF(SECOND, endeUhrzeitKonsens, NOW())) AS placematVorbeiSeitSekunden from placemat;", (err, rows) => {
        if (err) return next(err)         

        // Wenn es kein Placemat gibt:

        if(!rows.length){      
            
            res.render('admin.ejs', {        
                anzeigen: [],
                titel: "Mein Platzdeckchen",
                themen: "",
                urls: "",
                maxProGruppe: 6,
                dauerNachdenken: 5,
                dauerVergleichen: 5,
                dauerKonsens: 5,
                absenden: "absenden",
                placematUser: null
            })
        }else{                       

            let endeUhrzeitNachdenken = rows[0].endeUhrzeitNachdenken
            let endeUhrzeitVergleichen = rows[0].endeUhrzeitVergleichen
            let endeUhrzeitKonsens = rows[0].endeUhrzeitKonsens

            dbVerbindung.query("SELECT * FROM placematUser ORDER BY gruppe;", (err, result) => {            
                if (err) return next(err)
                
                let anzeigen = []            
                let button = ""
                // Falls das Placemat bereits abgelaufen ist:

                if(rows[0].placematVorbeiSeitSekunden > 0){
                    anzeigen.push("Zurzeit kein aktives Platzdeckchen.")                    
                    button = "Neu anlegen"
                }else{
                    
                    button = "Placemat aktiv"
                }
                anzeigen.push("<tr><td> Wann?</td><td>Was?</td></tr>") 
                anzeigen.push("<tr><td> jetzt</td><td>NACHDENKEN UND SCHREIBEN</td></tr>") 
                anzeigen.push("<tr><td> " + ("0" + endeUhrzeitNachdenken.getHours()).slice(-2) +":" + ("0" + endeUhrzeitNachdenken.getMinutes()).slice(-2) + " Uhr</td><td> STUMMES VERGLEICHEN</td></tr>")                 
                anzeigen.push("<tr><td> " + ("0" + endeUhrzeitVergleichen.getHours()).slice(-2) +":" + ("0" + endeUhrzeitVergleichen.getMinutes()).slice(-2) + " Uhr</td><td>TEILEN UND KONSENS FINDEN</td></tr>")                 
                anzeigen.push("<tr><td> " + ("0" + endeUhrzeitKonsens.getHours()).slice(-2) +":" + ("0" + endeUhrzeitKonsens.getMinutes()).slice(-2) + " Uhr</td><td>PÄSENTATION</td></tr>")
                

                console.log(rows[0].titel)

                res.render('admin.ejs', {
                    anzeigen: anzeigen,
                    titel: rows[0].titel,
                    themen: rows[0].themen,
                    urls: rows[0].urls,
                    maxProGruppe: rows[0]. maxProGruppe,
                    dauerNachdenken: rows[0].dauerNachdenken,
                    dauerVergleichen: rows[0].dauerVergleichen,
                    dauerKonsens: rows[0].dauerKonsens,
                    absenden: button,
                    placematUser: result
                })
            })
        }        
    })                
})

app.post('/admin', (req, res, next) => {    
    
    console.log("Admin-Button geklickt")
    
    dbVerbindung.query("SELECT COUNT(*) AS anzahl FROM users WHERE (username = '" + req.body.tbxUsername + "') AND (password = '" + req.body.tbxPassword + "');", (err,rows) => {                     
        if (err) return next(err)

        if (rows[0].anzahl === 1){

            placematUsers = []

            dbVerbindung.query("DELETE FROM placemat;", (err) => {        
                if (err) return next(err)
            })
        
            dbVerbindung.query("DELETE FROM placematUser;", (err) => {
                if (err) return next(err)
            })

            dbVerbindung.query("INSERT INTO placemat(titel, themen, urls, zeitstempel, maxProGruppe, dauerNachdenken, endeUhrzeitNachdenken, dauerVergleichen, endeUhrzeitVergleichen, dauerKonsens, endeUhrzeitKonsens) VALUES ('" + req.body.tbxTitel + "','" + req.body.tbxThemen + "','" + req.body.tbxUrls + "', now(), '" + req.body.tbxMaxProGruppe + "','" + req.body.tbxNachdenken + "', ADDTIME(now(), '0:" + req.body.tbxNachdenken + ":0'),'" + req.body.tbxVergleichen + "', ADDTIME(now(), '0:" + (parseInt(req.body.tbxVergleichen) + parseInt(req.body.tbxNachdenken)) + ":0'),'" + req.body.tbxKonsens + "', ADDTIME(now(), '0:" + (parseInt(req.body.tbxVergleichen) + parseInt(req.body.tbxNachdenken) + parseInt(req.body.tbxKonsens)) + ":0'));", (err) => {                     
                
                if (err) return next(err)
        
                res.render('admin.ejs', {            
                    anzeigen: [],
                    titel: req.body.tbxTitel,
                    themen: req.body.tbxThemen,
                    urls: req.body.urls,
                    maxProGruppe: req.body.tbxMaxProGruppe,
                    dauerNachdenken: req.body.tbxNachdenken,
                    dauerVergleichen: req.body.tbxVergleichen,
                    dauerKonsens: req.body.tbxKonsens,
                    absenden: "Platzdeckchen aktiv!",
                    placematUser: null
                })
            })    
        }else{            
            return next(new Error("Du hast keine Berechtigung dies zu tun!"))
        }
    })
})